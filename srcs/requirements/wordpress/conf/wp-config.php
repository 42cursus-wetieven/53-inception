<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

if (!function_exists('getenv_docker')) {
    function getenv_docker($env, $default) {
        // if ($fileEnv = getenv($env . '_FILE')) {
        //     return rtrim(file_get_contents($fileEnv), "\r\n");
        if (($val = getenv($env)) !== false) {
            return $val;
        } else {
            return $default;
        }
    }
}

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', getenv_docker('WORDPRESS_DB_NAME', 'mysql') );

/** MySQL database username */
define( 'DB_USER', getenv_docker('WORDPRESS_DB_USER', 'example username') );

/** MySQL database password */
define( 'DB_PASSWORD', getenv_docker('WORDPRESS_DB_PASSWORD', 'example password') );

/** MySQL hostname */
define( 'DB_HOST', getenv_docker('WORDPRESS_DB_HOST', 'mariadb') );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', getenv_docker('WORDPRESS_DB_CHARSET', 'utf8') );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', getenv_docker('WORDPRESS_DB_COLLATE', '') );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '@Q7CYgax:&-OXms<(%,cz`_-d-@@BX%=c:ya/7QBCmzD8H/G+&>$f} 2Mnq|a%T>');
define('SECURE_AUTH_KEY',  'n%i51suW!jMBy]pz~G<!E|/nC ~9#;D+]-NOa&H#e^WQk_8taq*xG%X{K |NZvVd');
define('LOGGED_IN_KEY',    'R2#dq,?3R<vNEdbe[?eXeUiHt )|V908##lNJL-Tge)}`-0zzT6Dc-ZTz>/9!Hg+');
define('NONCE_KEY',        'EMLhgoPl>VRrM~1Uuxw6:TA=ygX60Rcl)zL33<aE#j_B{~z c4z^q$i/+]_eLpfb');
define('AUTH_SALT',        'Rp2iIokcuo{kt:<Jr73o.VqK%KZ^*?K[`-KvDHP+|yWMH3Rx0L8WTk9q^~^GBT^T');
define('SECURE_AUTH_SALT', 'jU-PB@l7#lZ,kbPBUDzj_K@,+e:#ot`Yef{/76OuG+.cqZ~$Dn]FZrQy46,APeL:');
define('LOGGED_IN_SALT',   '<o#bZT`#u![%he[g||H!dKg62u.C.d<<bN|u/&..!:/Y3tftj VR!(W?)td=(#5Z');
define('NONCE_SALT',       '$sY66-5=pNMn[S|x$8-YVT[U-(1O_9IXy-ipV)_>URr2]m--Fidd.]Wv%C~gu]-+');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */

$table_prefix = getenv_docker('WORDPRESS_TABLE_PREFIX', 'wb_');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */

define( 'WP_DEBUG', !!getenv_docker('WORDPRESS_DEBUG', '') );

if ($configExtra = getenv_docker('WORDPRESS_CONFIG_EXTRA', '')) {
	eval($configExtra);
}

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
