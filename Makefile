docker-compose := srcs/docker-compose.yml

all:	build up

build:
		docker compose -f $(docker-compose) build 

up:
		docker compose -f $(docker-compose) up -d 

start:
		docker compose -f $(docker-compose) start 

down:
		docker compose -f $(docker-compose) down 

destroy:
		docker compose -f $(docker-compose) down -v 
		sudo rm -rf /home/wetieven/data/wp-db/*
		sudo rm -rf /home/wetieven/data/webserv-root/*

stop:
		docker compose -f $(docker-compose) stop 

restart:
		docker compose -f $(docker-compose) stop 
		docker compose -f $(docker-compose) up -d 

logs:
		docker compose -f $(docker-compose) logs --tail=100 -f 

ps:
		docker compose -f $(docker-compose) ps

.PHONY: all build up start down destroy stop restart logs ps
